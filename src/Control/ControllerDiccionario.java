package Control;

import Negocio.Diccionario;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

public class ControllerDiccionario {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblAvisoCarga;

    @FXML
    private Label lblURL;

    @FXML
    private Button cmdCargar;

    @FXML
    private Label lblAviso2Carga;

    @FXML
    private ImageView imgLupa;

    @FXML
    private TextArea txtReporteBuscar;

    @FXML
    private Button cmdBuscarPalabra;

    @FXML
    private TextField txtPalabraBuscar;

    @FXML
    private TextArea txtReporteImprimir;

    @FXML
    private Button cmdImprimirDiccionario;

    @FXML
    private Button cmdLimpiarImpresion;

    @FXML
    private TextField txtPatronBuscar;

    @FXML
    private TextField txtPatronNuevo;

    @FXML
    private Button cmdReemplazarPatron;

    @FXML
    private TextArea txtReporteReemplazar;

    @FXML
    private TextField txtNumeroImprimir;

    @FXML
    private Button cmdGenerarPdf;

    @FXML
    private Label lblReporte;

    @FXML
    private ImageView imgLogo;

    Diccionario diccionario = new Diccionario();

    @FXML
    void buscarPalabra(ActionEvent event) {
        String aviso = null;
        Alert alert = null;

        try {

            if (diccionario.buscarPalabra(txtPalabraBuscar.getText())) {

                txtReporteBuscar.setText("La palabra " + txtPalabraBuscar.getText() + " si está en el Diccionario.");
            } else {
                txtReporteBuscar.setText("La palabra " + txtPalabraBuscar.getText() + " no está en el Diccionario.");
            }
            
        } catch (Exception e) {
            aviso = "" + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();

        }
    }

    @FXML
    void cargarPalabras(ActionEvent event
    ) {
        String url = "https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt";
        diccionario.cargar(url);
        lblAvisoCarga.setText("¡Listo! Se han habilitado las demás funciones.");
        lblAviso2Carga.setText("Las palabras han sido cargadas desde la URL:");
        lblURL.setText(url);
        cmdBuscarPalabra.setDisable(false);
        cmdImprimirDiccionario.setDisable(false);
        cmdLimpiarImpresion.setDisable(false);
        cmdReemplazarPatron.setDisable(false);
        cmdGenerarPdf.setDisable(false);

    }

    @FXML
    void generarPdf(ActionEvent event) throws IOException {
        String aviso = null;
        Alert alert = null;
        try {
            int palabras = Integer.parseInt(txtNumeroImprimir.getText());
            String impresion = diccionario.getImprimirPdf(palabras);
            writePDF(impresion);
            aviso = "Tarea Completada";
            alert = new Alert(Alert.AlertType.INFORMATION, aviso, ButtonType.OK);
            alert.showAndWait();
        } catch (java.lang.NullPointerException e) {
            aviso = "" + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();
        } catch (java.lang.NumberFormatException x) {
            aviso = "Ingrese un numero valido ";
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();
        }

    }

    private static void writePDF(String impresion) throws IOException {

        Document document = new Document();

        try {
            String path = new File("./src/pdf").getCanonicalPath();
            String FILE_NAME = path + "/informe.pdf";

            PdfWriter.getInstance(document, new FileOutputStream(new File(FILE_NAME)));

            document.open();

            Paragraph diccionarioArray = new Paragraph();
            diccionarioArray.add(impresion);
            diccionarioArray.setAlignment(Element.ALIGN_JUSTIFIED);

            document.add(diccionarioArray);

            document.close();

        } catch (FileNotFoundException | DocumentException e) {

        }
    }

    @FXML
    void imprimirDiccionario(ActionEvent event) {
        txtReporteImprimir.setText(diccionario.getImprimir());
    }

    @FXML
    void limpiarimpresion(ActionEvent event) {
        txtReporteImprimir.setText("");
    }

    @FXML
    void reemplazarPatron(ActionEvent event) {
        String aviso = null;
        Alert alert = null;
        try {
            String buscar = txtPatronBuscar.getText();
            String cambiar = txtPatronNuevo.getText();
            int repeticiones = diccionario.buscarPatron(buscar, cambiar);
            txtReporteReemplazar.setText("El patron '" + buscar + "' se reemplazó por '" + cambiar + "' " + repeticiones + " veces.");
        } catch (Exception e) {
            aviso = "" + e.getMessage();
            alert = new Alert(Alert.AlertType.ERROR, aviso, ButtonType.OK);
            alert.showAndWait();

        }
    }

    @FXML
    void initialize() {
        assert lblAvisoCarga != null : "fx:id=\"lblAvisoCarga\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert lblURL != null : "fx:id=\"lblURL\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert cmdCargar != null : "fx:id=\"cmdCargar\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert lblAviso2Carga != null : "fx:id=\"lblAviso2Carga\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert imgLupa != null : "fx:id=\"imgLupa\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert txtReporteBuscar != null : "fx:id=\"txtReporteBuscar\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert cmdBuscarPalabra != null : "fx:id=\"cmdBuscarPalabra\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert txtPalabraBuscar != null : "fx:id=\"txtPalabraBuscar\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert txtReporteImprimir != null : "fx:id=\"txtReporteImprimir\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert cmdImprimirDiccionario != null : "fx:id=\"cmdImprimirDiccionario\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert cmdLimpiarImpresion != null : "fx:id=\"cmdLimpiarImpresion\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert txtPatronBuscar != null : "fx:id=\"txtPatronBuscar\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert txtPatronNuevo != null : "fx:id=\"txtPatronNuevo\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert cmdReemplazarPatron != null : "fx:id=\"cmdReemplazarPatron\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert txtReporteReemplazar != null : "fx:id=\"txtReporteReemplazar\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert txtNumeroImprimir != null : "fx:id=\"txtNumeroImprimir\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert cmdGenerarPdf != null : "fx:id=\"cmdGenerarPdf\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert lblReporte != null : "fx:id=\"lblReporte\" was not injected: check your FXML file 'Diccionario3.fxml'.";
        assert imgLogo != null : "fx:id=\"imgLogo\" was not injected: check your FXML file 'Diccionario3.fxml'.";

    }
}
