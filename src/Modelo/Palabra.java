/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 * Esta clase permite manejar una palabra a partir de un vector generico
 * de Character.
 * @author Jose Castillo, Felipe Sanguino josedavidcabu@ufps.edu.co,
 * andresfelipesasa@ufps.edu.co
 */
public class Palabra {
    
    private VectorGenerico<Character> caracteres;

    /**
     * Constructor vacío de Palabra.
     */
    public Palabra() {
        
    }

    /**
     * A partir de una cadena crea el vector genérico de caracteres
     * Ejemplo: Cadena="ufps", this.caracteres={"u","f","p","s"}
     *
     * @param cadena un String para introducir sus caracteres en un vector 
     * genérico
     */
    public Palabra(String cadena) {
        char h[] = cadena.toCharArray();
        this.caracteres = new VectorGenerico(h.length);
        for (int i = 0; i != this.caracteres.length(); i++){
            this.caracteres.add(h[i]);
        }
    }

    /**
     * Este metodo permite obtener el vector generico de caracteres de la 
     * palabra.
     * @return VectorGenerico de caracteres que componen la palabra.
     */
    public VectorGenerico<Character> getCaracteres() {
        return this.caracteres;
    }

    /**
     * Este metodo permite modificar el vector generico de caracteres que
     * componen una palabra
     *
     * @param caracteres Es un vector generico con las letras de una palabra
     */
    public void setCaracteres(VectorGenerico<Character> caracteres) {
        this.caracteres = caracteres;
    }

    /**
     * Este metodo busca un patron dentro del vector generico de la palabra 
     * y lo reemplaza por un nuevo patron especificado.
     *
     * @param patron Palabra que contiene el patron que se busca reemplazar
     * @param nuevoPatron Palabra que contiene el nuevo patron que sustituye al
     * primer parámetro.
     * @return int del número de cambios que se realizaron si se encontró el patrón.
     */
    public int buscarPatron(Palabra patron, Palabra nuevoPatron) {
        int numeroCambios = 0;
       
        for (int i = 0; this.caracteres.length() - i >= patron.caracteres.length(); i++) {

            int x = 0;
            boolean eq = true;
            if (this.caracteres.get(i).charValue() == patron.caracteres.get(x).charValue()) {
                int contador = i;
                while ( x != patron.caracteres.length() && eq) {
                    eq = (this.caracteres.get(contador).charValue() == patron.caracteres.get(x).charValue());
                    contador++;
                    x++;
                }
                if (eq) {
                    if (patron.caracteres.length() == nuevoPatron.caracteres.length()) {
                        int contador2 = i;
                        for (int y = 0; y != patron.caracteres.length()&& contador2 != contador; y++) {
                            this.caracteres.set(contador2, nuevoPatron.caracteres.get(y));
                            contador2++;
                            numeroCambios++;
                        }
                    } else {
                        VectorGenerico<Character> nuevo = new VectorGenerico(nuevoPatron.caracteres.length()
                                - patron.caracteres.length() + this.caracteres.length());
                        int z = 0;
                        while (z != i) {
                            nuevo.set(z, this.caracteres.get(z));
                            z++;
                        }
                        for (int h = 0; h != nuevoPatron.caracteres.length(); h++) {
                            nuevo.set(z, nuevoPatron.caracteres.get(h));
                            z++;
                        }
                        while (nuevo.length() > z && contador <= this.caracteres.length()) {
                            
                            nuevo.set(z, this.caracteres.get(contador));
                            contador++;
                            z++;
                        }
                        this.setCaracteres(nuevo);
                        numeroCambios++;
                    }
                }
            }
        }
        return numeroCambios;
    }

    /**
     * Método que permite obtener la palabra en mayuscula para la impresión en 
     * PDF.
     * @return String con la palabra en Mayuscula
     */
    public String impresionMayus() {
        String str = "";
        for (int i = 0; i != this.caracteres.length(); i++) {            
            str+=this.caracteres.get(i).toUpperCase(this.caracteres.get(i));           
        }
        return str;
    }

    /**
     * Este metodo compara si 2 objetos palabra son exactamente iguales.
     *
     * @param obj objeto que se quiere comparar
     * @return true si son iguales , false si no lo son.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Palabra other = (Palabra) obj;
        if (this.caracteres.length() != other.caracteres.length()) {
            return false;
        }
        boolean eq = false;
        for (int i = 0; i != this.caracteres.length() && !eq; i++) {
                
            eq = !(this.caracteres.get(i).charValue() == other.caracteres.get(i).charValue());
        }
        return !eq;
    }

    /**
     * Este metodo convierte a string el objeto palabra.
     *
     * @return String del objeto palabra.
     */
    @Override
    public String toString() {
        String salida = "";
        for (int i = 0; i != this.caracteres.length(); i++) {
            salida += this.caracteres.get(i).toString();
        }
        return salida;
    }
}
