/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Palabra;
import ufps.util.varios.ArchivoLeerURL;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 * Esta clase permite manejar un diccionario a partir de un vector generico de
 * Palabra.
 *
 * @author Jose Castillo, Felipe Sanguino josedavidcabu@ufps.edu.co ,
 * andresfelipesasa@ufps.edu.co
 */
public class Diccionario {

    private VectorGenerico<Palabra> palabras;

    /**
     * Constructor vacío de Diccionario
     */
    public Diccionario() {
    }

    /**
     * A partir de una url, carga las palabras para almacenarlas en el vector
     * generico.
     *
     * @param urlDiccionario String que contiene la URL del archivo de texto
     * plano
     */
    public void cargar(String urlDiccionario) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(urlDiccionario);
        Object datos[] = archivo.leerArchivo();
        this.palabras = new VectorGenerico(datos.length);
        for (int i = 0; i != this.palabras.length(); i++) {
            this.palabras.add(new Palabra((String) datos[i]));

        }
    }

    /**
     * Retorna true o false si la palabra se encuentra en el vector genérico.
     *
     * @param palabra_A_buscar String de la palabra que se quiere buscar.
     * @return True si la palabra se encuentra en el diccionario y false en caso
     * contrario
     */
    public boolean buscarPalabra(String palabra_A_buscar) {
        //Debe convertir la palabra_A_buscar a un objeto palabra:
        if (palabra_A_buscar.isEmpty() || "\u0020".equals(palabra_A_buscar)) {
            throw new RuntimeException("El caracter es vacío ó es un espacio, verifique.");
        } else {

            Palabra myPalabra = new Palabra(palabra_A_buscar);
            for (int i = 0; this.palabras.length() != i; i++) {
                if (this.palabras.get(i).equals(myPalabra)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Este metodo busca un patron dentro de cada una de las palabras contenidas
     * en el vectorGenerico de palabras
     *
     * @param patron Es un String del parametro que se quiere buscar
     * @param patron_Nuevo Es el String por el cual se reemplaza el patron
     * @return El número de veces que se reemplazó el patron.
     */
    public int buscarPatron(String patron, String patron_Nuevo) {
        if ((patron_Nuevo.isEmpty() || "\u0020".equals(patron_Nuevo) || "\u0020".equals(patron) || patron.isEmpty())) {
            throw new RuntimeException("El caracter es vacío ó es un espacio, verifique.");
        } else {

            int contador = 0;
            Palabra myPatron = new Palabra(patron);
            Palabra myPatron_Nuevo = new Palabra(patron_Nuevo);
            for (int i = 0; i != this.palabras.length(); i++) {
                contador += this.palabras.get(i).buscarPatron(myPatron, myPatron_Nuevo);
            }
            if (contador == 0) {
                return -1;
            }
            return contador;
        }
    }

    /**
     * Retorna la información contenida en el diccionario
     *
     * @return un String con todas las palabras que contiene el diccionario
     */
    public String getImprimir() {
        String str = "";
        for (int i = 0; i < this.palabras.length(); i++) {
            str += this.palabras.get(i).toString() + "\n";
        }
        return str;
    }

    /**
     * este metodo retorna un string con el total de palabras indicadas 
     * @param x es el numero de palabras que se quieren imprimir en ped
     * @return un string con la cantidad de palabras indicadas 
     */
    public String getImprimirPdf(int x) {
        if (x >=this.palabras.length()) {
            throw new java.lang.NullPointerException("El numero supera el total de palabras del Diccionario");
        } else {
            String str = "PDF CREADO CON " + x + " PALABRAS" + "\n" + "\n" + "\n";

            for (int i = 0; i != x+1; i++) {

                str += "[" + (i + 1) + "] " + this.palabras.get(i).impresionMayus() + "\n";
            }
            return str;
        }
    }

}
